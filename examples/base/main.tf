module "hcloud_k3s" {
  source = "../../"

  enable_multi_master = false
  master_type         = "cax11"
  network_range       = "10.0.0.0/8"
  network_subnet      = "10.11.12.0/24"
  node_type           = "cax11"

  node_groups         = [
    {
      name  = "base"
      type  = "cax11"
      nodes = 2
    }
  ]
}

data "external" "hcloud_token" {
  program = ["sh", "-c", <<EOT
echo {\"token\":\"$HCLOUD_TOKEN\"}
EOT
]
}

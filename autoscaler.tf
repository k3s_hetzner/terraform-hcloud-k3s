resource "terraform_data" autoscaler {

  triggers_replace = [
    terraform_data.hcloud_k3s_master[0].id,
    var.auto_node_groups,
    var.cluster_autoscaler_version,
  ]

  connection {
    user        = "ubuntu"
    private_key = tls_private_key.hcloud_k3s_key.private_key_pem
    host        = hcloud_server.hcloud_k3s_master[0].ipv4_address
    port        = local.ssh_port
  }

  provisioner "file" {
    content = templatefile("${path.module}/files/cluster_autoscaler.tpl.yaml", {
      cluster_autoscaler_version  = var.cluster_autoscaler_version,
      hcloud_image                = var.image,
      hcloud_network              = hcloud_network.hcloud_k3s_network.id,
      default_location            = var.location,
      auto_node_groups            = jsonencode(var.auto_node_groups),
      cloud_init                  = base64encode(templatefile("${path.module}/files/node_cloud_config.tpl.yaml", {
                                      key             = tls_private_key.hcloud_k3s_key.public_key_openssh,
                                      k3s_channel     = var.k3s_channel,
                                      k3s_token       = random_string.k3s_token.result,
                                      k3s_private_ip  = local.k3s_private_ip,
                                      is_intel        = startswith(var.node_type, "cx") || (startswith(var.node_type, "ccx") && endswith(var.node_type, "1"))
      }))
    })
    destination = "/home/ubuntu/autoscaler.yaml"
  }

  provisioner "remote-exec" {
    inline = [
      "cd",
      "kubectl apply -f autoscaler.yaml --dry-run=client -o yaml | kubectl apply -f -",
    ]
  }
}

resource "terraform_data" auto_nodes {

  for_each = { for item in var.auto_node_groups : item.name => item }

  provisioner "local-exec" {
    when    = destroy
    command = "hcloud server list -o=noheader -o=columns=name | grep ${each.key} | xargs -L1 -I{} hcloud server delete {}"
  }
}

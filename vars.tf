variable hcloud_token {
  default = ""
}

variable placement_group_name {
  default = "hcloud_k3s_placement_group"
}

variable network_name {
  default = "hcloud_k3s_network"
}

variable network_range {
  default = "172.16.0.0/12"
}

variable network_subnet {
  default = "172.16.1.0/24"
}

variable master_type {
  default = "cx11"
}

variable node_type {
  default = "cx11"
}

variable image {
  default = "ubuntu-22.04"
}

variable location {
  default = "fsn1"
}

variable master_name {
  default = "master"
}

variable master_name_format {
  default = "master-%d"
}

variable enable_multi_master {
  default = false
}

variable node_groups {
  type      = list(object({
    name      = string
    type      = string
    location  = optional(string)
    nodes     = number
  }))
  default   = []
}

variable auto_node_groups {
  type      = list(object({
    name      = string
    type      = string
    location  = optional(string)
    min       = number
    max       = number
  }))
  default   = []
}

variable private_key {
  type    = string
  default = null
}

variable public_key {
  type    = string
  default = null
}

variable cluster_name {
  default = "k3s"
}

variable cluster_cidr {
  default = "10.42.0.0/16"
}

variable kubeconfig_location {
  type    = string
  default = null
}

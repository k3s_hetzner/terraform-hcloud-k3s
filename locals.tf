locals {
  ssh_args                  = "-T -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o IdentitiesOnly=yes -o PubkeyAuthentication=yes -i ${local_sensitive_file.hcloud_k3s_private_key_file.filename}"
  ssh_port                  = 22

  master_public_ip          = length(hcloud_load_balancer.master) > 0? hcloud_load_balancer.master[0].ipv4 : hcloud_server.hcloud_k3s_master[0].ipv4_address
  master_private_ip         = cidrhost(var.network_subnet, 1)
  load_balancer_private_ip  = cidrhost(var.network_subnet, -2)
  load_balancer_public_ip   = length(hcloud_load_balancer.master) > 0? hcloud_load_balancer.master[0].ipv4 : 0
  k3s_private_ip            = length(hcloud_load_balancer.master) > 0? cidrhost(var.network_subnet, -2) : cidrhost(var.network_subnet, 1)

  worker_nodes = flatten([
    for group in var.node_groups : [
      for i in range(1, group.nodes + 1) : {
        name = "${group.name}${format("%02d", i)}"
        type = group.type
      }
    ]
  ])

  kubeconfig_external       = replace(data.remote_file.kubeconfig.content, "127.0.0.1", local.master_public_ip)
}

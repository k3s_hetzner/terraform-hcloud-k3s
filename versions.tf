variable k3s_channel {
  default = "stable"
}

variable hcloud_csi_version {
  default = "2.6.0"
}

variable cluster_autoscaler_version {
  default = "1.29.2"
}

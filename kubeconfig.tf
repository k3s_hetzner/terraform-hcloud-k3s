resource "local_sensitive_file" kubeconfig {

  depends_on = [
    terraform_data.hcloud_k3s_master
  ]

  content         = local.kubeconfig_external
  filename        = var.kubeconfig_location != null ? var.kubeconfig_location : "${path.module}/${var.cluster_name}_kubeconfig"
  file_permission = "600"
}

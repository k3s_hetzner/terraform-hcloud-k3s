data "external" hcloud_token {
  program = ["sh", "-c", <<EOT
echo {\"token\":\"$HCLOUD_TOKEN\"}
EOT
]
}

data "template_file" node_cloud_config {
  template  = file("${path.module}/files/node_cloud_config.tpl.yaml")
  vars      = {
    key             = tls_private_key.hcloud_k3s_key.public_key_openssh,
    k3s_channel     = var.k3s_channel,
    k3s_token       = random_string.k3s_token.result,
    k3s_private_ip  = local.k3s_private_ip,
    is_intel        = startswith(var.node_type, "cx") || (startswith(var.node_type, "ccx") && endswith(var.node_type, "1"))
  }
}

data "template_file" master_cloud_config {
  template  = file("${path.module}/files/master_cloud_config.tpl.yaml")
  vars      = {
    key                 = tls_private_key.hcloud_k3s_key.public_key_openssh,
    k3s_channel         = var.k3s_channel,
    k3s_token           = random_string.k3s_token.result,
    k3s_lb_public_ip    = local.load_balancer_public_ip,
    k3s_private_ip      = local.master_private_ip,
    instance_type       = var.master_type,
    is_intel            = startswith(var.master_type, "cx") || (startswith(var.master_type, "ccx") && endswith(var.master_type, "1"))
    count               = count.index,
    hcloud_token        = data.external.hcloud_token.result.token,
    hcloud_network      = hcloud_network.hcloud_k3s_network.id,
    hcloud_csi_version  = var.hcloud_csi_version,
    cluster_cidr        = var.cluster_cidr,
  }

  count                 = var.enable_multi_master? 3: 1
}

data "cloudinit_config" node_cloud_init_config {
  gzip          = false
  base64_encode = false

  part {
    filename      = "cloud_config.yaml"
    content_type  = "text/cloud-config"

    content       = data.template_file.node_cloud_config.rendered
  }

}

data "cloudinit_config" master_cloud_init_config {
  gzip          = false
  base64_encode = false

  part {
    filename      = "cloud_config.yaml"
    content_type  = "text/cloud-config"

    content       = data.template_file.master_cloud_config[count.index].rendered
  }

  count         = var.enable_multi_master? 3: 1
}

data "remote_file" kubeconfig {
  depends_on = [terraform_data.hcloud_k3s_master[0]]

  conn {
    host        = hcloud_server.hcloud_k3s_master[0].ipv4_address
    user        = "ubuntu"
    private_key = tls_private_key.hcloud_k3s_key.private_key_openssh
  }

  path = "/etc/rancher/k3s/k3s.yaml"
}

resource "random_string" k3s_token {
  length  = 48
  special = false
}

resource "tls_private_key" hcloud_k3s_key {
  algorithm = "ED25519"
}

resource "local_sensitive_file" hcloud_k3s_private_key_file {
  content               = tls_private_key.hcloud_k3s_key.private_key_openssh
  filename              = var.private_key != null ? var.private_key : "${path.module}/${var.cluster_name}_key"
  file_permission       = "400"
  directory_permission  = "700"
}

resource "local_sensitive_file" hcloud_k3s_public_key_file {
  content               = tls_private_key.hcloud_k3s_key.public_key_openssh
  filename              = var.public_key != null ? var.public_key : "${path.module}/${var.cluster_name}_key.pub"
  file_permission       = "600"
  directory_permission  = "700"
}

resource "hcloud_server" hcloud_k3s_master {

  depends_on          = [
    hcloud_network_subnet.hcloud_k3s_subnet
  ]

  name                = format(var.master_name_format, count.index + 1)

  server_type         = var.master_type
  image               = var.image
  location            = var.location
  placement_group_id  = hcloud_placement_group.hcloud_k3s_placement_group.id
  user_data           = data.cloudinit_config.master_cloud_init_config[count.index].rendered

  network {
    network_id  = hcloud_network.hcloud_k3s_network.id
    ip          = cidrhost(var.network_subnet, count.index + 1)
  }

  labels = {
    group = "master"
  }

  count               = var.enable_multi_master? 3: 1
}

resource "terraform_data" hcloud_k3s_master {

  triggers_replace = [
    hcloud_server.hcloud_k3s_master[count.index].id
  ]

  connection {
    user        = "ubuntu"
    private_key = tls_private_key.hcloud_k3s_key.private_key_pem
    host        = hcloud_server.hcloud_k3s_master[0].ipv4_address
    port        = local.ssh_port
  }

  provisioner "remote-exec" {
    inline = [
      "systemctl is-system-running --wait",
    ]
    on_failure = continue
  }

  provisioner "remote-exec" {
    inline = [
      "systemctl is-system-running --wait",
      "kubectl get nodes",
      "kubectl get pods -A",
    ]
  }

  provisioner "local-exec" {
    command = "[ -e ${local_sensitive_file.hcloud_k3s_private_key_file.filename} ] && rm -f ${local_sensitive_file.hcloud_k3s_private_key_file.filename} || true"
    quiet   = true
  }

  provisioner "local-exec" {
    command = "[ -e ${local_sensitive_file.hcloud_k3s_public_key_file.filename} ] && rm -f ${local_sensitive_file.hcloud_k3s_public_key_file.filename} || true"
    quiet   = true
  }

  count = var.enable_multi_master? 3: 1
}

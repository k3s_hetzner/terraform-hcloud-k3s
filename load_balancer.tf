resource "hcloud_load_balancer" master {
  name                = "load-balancer-master"
  load_balancer_type  = "lb11"
  location            = var.location
  count               = var.enable_multi_master? 1: 0
}

resource "hcloud_load_balancer_network" master {
  load_balancer_id  = hcloud_load_balancer.master[0].id
  subnet_id         = hcloud_network_subnet.hcloud_k3s_subnet.id
  ip                = local.load_balancer_private_ip
  count             = var.enable_multi_master? 1: 0
}

resource "hcloud_load_balancer_service" master {
  load_balancer_id  = hcloud_load_balancer.master[0].id
  protocol          = "tcp"
  listen_port       = 6443
  destination_port  = 6443
  count             = var.enable_multi_master? 1: 0
}

resource "hcloud_load_balancer_target" master {
  depends_on = [
    hcloud_load_balancer_network.master,
    terraform_data.hcloud_k3s_master,
  ]

  type              = "label_selector"
  load_balancer_id  = hcloud_load_balancer.master[0].id
  label_selector    = "k3s=master"
  use_private_ip    = true
  count             = var.enable_multi_master? 1: 0
}

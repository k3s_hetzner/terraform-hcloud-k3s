resource "hcloud_server" hcloud_k3s_nodes {

  depends_on          = [
    terraform_data.hcloud_k3s_master,
    hcloud_network_subnet.hcloud_k3s_subnet,
  ]


  for_each = { for worker_node in local.worker_nodes : worker_node.name => worker_node }

  name                = each.key
  server_type         = each.value.type
  image               = "ubuntu-22.04"
  location            = var.location

  placement_group_id  = hcloud_placement_group.hcloud_k3s_placement_group.id
  user_data           = data.cloudinit_config.node_cloud_init_config.rendered

  network {
    network_id  = hcloud_network.hcloud_k3s_network.id
  }

  labels = {
    group = split("-", each.key)[0]
  }
}

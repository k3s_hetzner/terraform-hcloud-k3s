terraform {
  required_version = ">= 1.4, < 2.0"

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">= 1.47"
    }
    remote = {
      source = "tenstad/remote"
      version = ">= 0.1.3"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token == "" ?  data.external.hcloud_token.result.token : var.hcloud_token
}

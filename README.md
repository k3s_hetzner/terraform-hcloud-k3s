# Terraform module - k3s in Hetzner Cloud

## Description

This module builds a k3s cluster in Hetzner Cloud, managing the worker nodes with nodegroups managed by Cluster Autoscaler and nodegroups with specific type and size.

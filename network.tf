resource "hcloud_network" hcloud_k3s_network {
  name      = var.network_name
  ip_range  = var.network_range
}

resource "hcloud_network_subnet" hcloud_k3s_subnet {
  network_id    = hcloud_network.hcloud_k3s_network.id
  ip_range      = var.network_subnet
  type          = "server"
  network_zone  = "eu-central"
}

resource "hcloud_placement_group" hcloud_k3s_placement_group {
  name = var.placement_group_name
  type = "spread"
}
